package tetris

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"reflect"
)

func permutations(arr []string)[][]string{
	var helper func([]string, int)
	res := [][]string{}

	helper = func(arr []string, n int){
		if n == 1{
			tmp := make([]string, len(arr))
			copy(tmp, arr)
			res = append(res, tmp)
		} else {
			for i := 0; i < n; i++{
				helper(arr, n - 1)
				if n % 2 == 1{
					tmp := arr[i]
					arr[i] = arr[n - 1]
					arr[n - 1] = tmp
				} else {
					tmp := arr[0]
					arr[0] = arr[n - 1]
					arr[n - 1] = tmp
				}
			}
		}
	}
	helper(arr, len(arr))
	return res
}

func Memory(fPath string) {

	file, err := os.Open(fPath)
	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	var strings [] string
	for scanner.Scan() {
		strings = append(strings, scanner.Text())
	}
	err = file.Close()
	if err != nil {
		return
	}

	var structs []string
	var i int

	var fileBeforeStructs []string
	var fileAfterStructs []string

	for _, eachline := range strings {
		// i = 2 after structs
		// i = 1 structs
		// i = 0 before structs
		if i == 2 {
			fileAfterStructs = append(fileAfterStructs, eachline)
		}

		if eachline == "}" && i == 1 {
			fileAfterStructs = append(fileAfterStructs, eachline)
			i = 2
		}

		if i == 1 {
			structs = append(structs, eachline)
		}

		if eachline == "type Tetris struct {" && i == 0 {
			fileBeforeStructs = append(fileBeforeStructs, eachline)
			i = 1
		}

		if i == 0 {
			fileBeforeStructs = append(fileBeforeStructs, eachline)
		}
	}
	fmt.Println("Before structs: ")
	fmt.Println(fileBeforeStructs)
	fmt.Println()
	fmt.Println("Structs: ")
	fmt.Println(structs)
	fmt.Println()
	fmt.Println("After Structs: ")
	fmt.Println(fileAfterStructs)

	permutations := permutations(structs)
	//fmt.Println(permutations)

	for _, val := range permutations {
		err = os.Remove(fPath)
		if err != nil {
			log.Fatalf("failed deleting file: %s", err)
		}

		file, err = os.OpenFile(fPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)

		if err != nil {
			log.Fatalf("failed creating file: %s", err)
		}

		datawriter := bufio.NewWriter(file)

		for _, data := range fileBeforeStructs {
			_, _ = datawriter.WriteString(data + "\n")
		}

		for _, data := range val {
			_, _ = datawriter.WriteString(data + "\n")
		}

		for _, data := range fileAfterStructs {
			_, _ = datawriter.WriteString(data + "\n")
		}

		err = datawriter.Flush()
		if err != nil {
			return
		}

		err = file.Close()
		if err != nil {
			return
		}

		//v := Tetris{}
		//fmt.Printf("Type someData is %d bytes long\n", unsafe.Sizeof(v))
		//SizeOfStruct()
		//fmt.Println(val)
		//SizeOfStruct()
		v := Tetris{}
		typ := reflect.TypeOf(v)
		//fmt.Println(val)
		fmt.Printf("Type someData is %d bytes long\n", typ.Size())
	}
}
