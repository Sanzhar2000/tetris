package tetris

type Tetris struct {
	FirstField int
	SecondField *bool
	ThirdField string
	FourthField uint64
	FifthField string
	SixthField *byte
}

//func SizeOfStruct()  {
//	v := Tetris{}
//	//typ := reflect.TypeOf(v)
//	//fmt.Printf("Type someData is %d bytes long\n", typ.Size())
//	fmt.Printf("Type someData is %d bytes long\n", unsafe.Sizeof(v))
//}
